<?php

use GuzzleHttp\Client;

require __DIR__ . '/vendor/autoload.php';

$client = new Client();
$res = $client->request('POST', 'http://localhost:8080/import', [
    'auth' => ['itss', 'itss'],
    GuzzleHttp\RequestOptions::JSON => [
        'packagesUrl' => [
            'http://workspace.localhost/wd/examplePackage.zip'
        ]
    ]
]);
echo $res->getBody()->getContents() . "\n";
