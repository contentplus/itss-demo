import {amdModuleLoader, ExerciseApi, ModuleLoader} from './lib/exercise-runtime'
import ModulePreload from "./lib/ModulePreload";
import DataStorage from "./lib/DataStorage";
import ExerciseResultsCollector from "./lib/ExerciseResultsCollector";
import EventBus from "./lib/EventBus";
import axios from 'axios';
import './style/main.scss'

import $ from 'jquery';

window.$ = window.jQuery = $;

import 'jquery-ui-bundle';

amdModuleLoader.registerInterceptor(function (context, moduleName, url) {
    let result = (function (moduleName) {
        switch (moduleName) {
            case 'jqueryui':
                return Promise.resolve(null);
            case 'amd-module-loader':
                return Promise.resolve(amdModuleLoader);
            case 'vue':
                return import('vue');
            case 'vue-router':
                return import('vue-router');
            case 'vuex':
                return import('vuex');
            case 'jquery':
            case 'jquery:2':
                return import('jquery');
            case 'underscore':
                return import('underscore');
            case 'backbone':
                return import('backbone');
            case 'react':
            case 'react:16':
                return import('react');
            case 'axios':
                return import('axios');
        }
    })(moduleName);

    if (result) {
        console.debug('Loading module', moduleName);
        return result;
    }
});

let storage = new DataStorage(),
    exerciseResultsCollector = new ExerciseResultsCollector(),
    eventBus = new EventBus(),
    exerciseApiOptions = {
        manifestResolver: (code, Axios) => {
            let dataPath = '/data/resource/' + code + '/';
            return Axios.get(dataPath + 'manifest.json').then(response => {
                return {
                    dataPath: dataPath,
                    manifest: response.data
                }
            });
        }
    };

let apiFactory = (module) => {
    return new ExerciseApi(
        module,
        storage,
        exerciseResultsCollector,
        eventBus,
        module.getStorageKey(),
        module.getCssPrefix(),
        moduleLoader,
        exerciseApiOptions
    )
};

let moduleLoader = new ModuleLoader(apiFactory, new ModulePreload());
moduleLoader.register((engine) => {
    let enginePath = '/engine/' + engine;
    return axios
        .get(enginePath + '/engine.json')
        .then((response) => {
            return {
                baseUrl: enginePath + '/',
                manifest: response.data
            };
        })
});

let storageId = 'demo2';

function getContrastModeName(value) {
    switch (value) {
        case 1:
            return 'yellowOnBlack';
        case 2:
            return 'blackOnYellow';
        case 3:
            return 'whiteOnBlack';
        default:
            return value
    }
}

let appContainer = document.getElementById('app');

Promise.all(
    window.TEST_MANIFEST.resources.map((resource, i) => {
        let header = document.createElement('h1');
        header.textContent = 'Zadanie nr. ' + (i + 1);
        appContainer.append(header);

        let div = document.createElement('div');
        appContainer.append(div);

        div.setAttribute('data-index', i);
        div.setAttribute('data-code', resource.code);

        let dataPath = window.TEST_EXTRACTED_RESOURCES_PATH + resource.code;

        return axios.get(dataPath + '/manifest.json')
            .then((response) => {
                let manifest = response.data;

                return moduleLoader.initModule(
                    manifest,
                    dataPath,
                    div,
                    Object.assign({
                        locale: 'pl',
                        contrastMode: getContrastModeName(0),
                        showAnswers: true,
                        isLoggedIn: false,
                        generatingPrintPdf: false
                    }),
                    storageId
                ).then((module) => {
                    console.log('Module has been loaded', module);
                    module.resize();
                });
            });
    })
).then(() => {
    console.log('Ready');
})
