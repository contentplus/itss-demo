export default class EventBus {
    emit(event, data) {
        console.debug('EventBus.emit()', event, data);
    }
}
