export default class DataStorage {
    load(key, callback) {
        callback(localStorage.getItem(key));
    }

    store(key, value, callback) {
        localStorage.setItem(key, value);
        callback();
    }

    canUpload() {
        return false;
    }
}
