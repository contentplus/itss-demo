<?php

use GuzzleHttp\Client;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

require __DIR__ . '/vendor/autoload.php';

$log = new Logger('name');
$log->pushHandler(new StreamHandler(__DIR__ . '/public/data/application.log', Logger::DEBUG));

$filesystem = new Filesystem();

$localPath = __DIR__ . '/tmp/' . uniqid(true) . '.zip';
$unzipPath = $localPath . '_extracted/';

$pid = getmypid();

try {
    $packageUrl = $argv[1];
    $log->info("[{$pid}] Import {$packageUrl}");

    $client = new Client();
    $res = $client->get($packageUrl, [
        'sink' => $localPath
    ]);

    if (!is_file($localPath)) {
        throw new \RuntimeException('Cannot download file');
    }

    mkdir($unzipPath, 0777, true);

    $process = (new Process([
        'unzip',
        $localPath,
        '-d',
        $unzipPath . '/'
    ]));
    $process->mustRun();

    $manifest = json_decode(file_get_contents($unzipPath . '/testManifest.json'), true);

    $output = __DIR__ . '/public/data/manifest/' . $manifest['project']['code'] . '.json';
    $filesystem->remove($output);
    $filesystem->copy(
        $unzipPath . '/testManifest.json',
        $output
    );

    foreach ($manifest['resources'] as $resource) {

        $resourceOutput = $output = __DIR__ . '/public/data/resource/' . $resource['code'];
        $filesystem->remove($resourceOutput);
        mkdir($resourceOutput, 0777, true);

        $process = (new Process([
            'unzip',
            $unzipPath . '/' . $resource['file'],
            '-d',
            $resourceOutput
        ]));
        $process->mustRun();
    }

    $log->info("[{$pid}] Import {$packageUrl} ... done");

} catch (Throwable $e) {
    $log->error('Unable to import', [
        'exception' => $e
    ]);
} finally {
    $filesystem->remove($localPath);
    $filesystem->remove($unzipPath);
}
