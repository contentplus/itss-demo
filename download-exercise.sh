#!/bin/bash

API_TOKEN=cNWpveBypPPGlZiwZzFRpfTKocS7slZ99bQL7cQEWqhyrA9QWvxgZqetIqmRGDrh;

cat ./engine-list.txt | while read line ; do
echo "$line"
  RESPONSE=$(curl -s --header "Authorization: ApiToken $API_TOKEN" $line)
  EXERCISE_ID=$(echo $RESPONSE | jq -r .id)
  EXERCISE_DOWNLOAD_URL=$(echo $RESPONSE | jq -r .deployment.packageDownloadUrl)

  rm -r "public/engine/${EXERCISE_ID}"
  mkdir -p "public/engine/${EXERCISE_ID}"
  curl -L "${EXERCISE_DOWNLOAD_URL}" --output tmp.zip
  unzip tmp.zip -d "public/engine/${EXERCISE_ID}"
  rm tmp.zip
done
