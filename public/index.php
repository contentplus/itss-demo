<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Middleware\BodyParsingMiddleware;
use Symfony\Component\Process\Process;
use Tuupola\Middleware\HttpBasicAuthentication;

require __DIR__ . '/../vendor/autoload.php';

$log = new \Monolog\Logger('name');
$log->pushHandler(new \Monolog\Handler\StreamHandler(__DIR__ . '/data/application.log', \Monolog\Logger::DEBUG));

$app = AppFactory::create();

$app->addErrorMiddleware(true, true, true);

$app->add(new BodyParsingMiddleware());

$app->add(new HttpBasicAuthentication([
    'path' => '/api/',
    "secure" => false,
    'users' => [
        "itss" => "itss",
    ]
]));

$app->get('/', function (Request $request, Response $response, array $args) {
    $links = [];
    foreach (glob(__DIR__ . '/data/manifest/*.json') as $file) {
        $manifest = json_decode(file_get_contents($file), true);
        $manifest['project']['name'] = htmlentities($manifest['project']['name']);
        $links[] = "<a href='/b/{$manifest['project']['code']}'>{$manifest['project']['name']}</a>";
    }

    $response->getBody()->write(join('<br/>', $links));

    return $response;
});

$app->get('/b/{project}', function (Request $request, Response $response, array $args) {
    $manifest = json_decode(file_get_contents(__DIR__ . '/data/manifest/' . $args['project'] . '.json'), true);

    $manifestJson = json_encode($manifest);
    $response->getBody()->write(
        <<<EOF
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="utf-8"/>
    <title>Demo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, User-scalable=no, maximum-scale=1">
    <link type="text/css" rel="stylesheet" href="/dist/appLoadTestExercises.css">
    <script>
    MathJax = {
        startup: {
            typeset: false
        },
        options: {
            enableMenu: false,
            menuOptions: {
                settings: {
                    inTabOrder: false
                },
            }
        },
        chtml: {
            scale: 1.125,
            adaptiveCSS: false
        }
    };
    </script>
    <script type="text/javascript" id="MathJax-script"
            src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
</head>
<body>
<script>
    window.TEST_EXTRACTED_RESOURCES_PATH = '/data/resource/'
    window.TEST_MANIFEST = $manifestJson;
</script>
<div id="app"></div>
<script src="/dist/appLoadTestExercises.js"></script>
</body>
</html>
EOF
    );

    return $response;
});

$app->post('/import', function (Request $request, Response $response, array $args) use ($log) {
    $log->info("Incoming request: {$request->getBody()->getContents()}");

    $body = $request->getParsedBody();

    foreach ($body['Urls'] as $packageUrl) {
        queuePackageImport($packageUrl, $log);
    }

    $response->getBody()->write(
        json_encode([
            'status' => 'ok'
        ])
    );
    return $response->withHeader('Content-type', 'application/json');
});

function queuePackageImport($packageUrl, \Monolog\Logger $log)
{
    $process = new Process([
        'nohup',
        'php',
        __DIR__ . '/../import.php',
        $packageUrl
    ]);
    $process->setOptions([
        'create_new_console' => true
    ]);
    $process->start();
    $log->info("Process has been started: {$process->getCommandLine()}");
}

$app->run();
